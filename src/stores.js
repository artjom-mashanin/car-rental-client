import { writable } from 'svelte/store';
import apiClient from './api';

export const error = writable(null);
export const setError = e => error.update(() => e);
export const resetError = () => error.update(() => null);

export const token = writable(window.localStorage.getItem('token') || null);
export const setToken = jwt => {
	window.localStorage.setItem('token', jwt);
	token.update(() => jwt);
};
export const user = writable(null);
export const setUser = data => user.update(() => data);

export const getUser = async () => {
	const userData = await apiClient.getUser();

	setUser(userData);
};

export const signup = async data => {
	const response = await apiClient.signup(data);

	setUser(response.user);
	setToken(response.token);
};

export const login = async data => {
	const response = await apiClient.login(data);

	setUser(response.user);
	setToken(response.token);
};

export const logout = () => {
	setUser(null);
	setToken('');
};
