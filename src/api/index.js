import axios from 'axios';
import { error, resetError } from '../stores';

const BASE_URL = 'http://localhost:4343';

const token = window.localStorage.getItem('token');

const headers = token ? { Authorization: `Bearer ${token}` } : null;

const api = axios.create({
	baseURL: BASE_URL,
	headers,
});

const apiClient = {
	async getCars() {
		try {
			const { data } = await api.get('/cars');

			resetError();

			return data;
		} catch (err) {
			error.update(() => 'Unauthorized');
			throw err;
		}
	},

	async signup(formData) {
		try {
			const response = await api.post('/users/signup', formData);
			const { data } = response;

			resetError();

			return data;
		} catch (err) {
			console.log('Err.message', err.message);
			error.update(() => err.message);
		}
	},

	async login(formData) {
		try {
			const response = await api.post('/users/login', formData);
			const { data } = response;

			resetError();

			return data;
		} catch (err) {
			error.update(() => 'Unauthorized');
			throw err;
		}
	},

	async getUser() {
		try {
			const response = await api.get('/users/self');
			const { user } = response.data;

			resetError();

			return user;
		} catch (err) {
			throw err;
		}
	},
};

export default apiClient;
