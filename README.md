# Front End App for Car RENTAL
## Project description
* App build using Svelte.js
* It's far from being finished
* Currently implemented features are: signup, login, logout, car list (I think that's it for now :man-shrugging:)
* Will keep adding stuff here, as Svelte seems like a really nice technology to play with and I wanna keep exploring it

## How to run
* Run `npm run dev` - this will spin up the development server accessible on `localhost:5000`

## Below comes color pallete I chose for the project and also some default Readme stuff from svelte


COLORS:
DARK_BLUE = #1d4d5d
GREEN = #01b7ab
LIGHT_GRAY = #e7ebf4
YELLOW = #fcedda
TURQOUISE?? = #d7efef
GRAY_BLUE = #6a5768

*Psst — looking for a shareable component template? Go here --> [sveltejs/component-template](https://github.com/sveltejs/component-template)*

---

# svelte app

This is a project template for [Svelte](https://svelte.dev) apps. It lives at https://github.com/sveltejs/template.

To create a new project based on this template using [degit](https://github.com/Rich-Harris/degit):

```bash
npx degit sveltejs/template svelte-app
cd svelte-app
```

*Note that you will need to have [Node.js](https://nodejs.org) installed.*


## Get started

Install the dependencies...

```bash
cd svelte-app
npm install
```

...then start [Rollup](https://rollupjs.org):

```bash
npm run dev
```

Navigate to [localhost:5000](http://localhost:5000). You should see your app running. Edit a component file in `src`, save it, and reload the page to see your changes.

By default, the server will only respond to requests from localhost. To allow connections from other computers, edit the `sirv` commands in package.json to include the option `--host 0.0.0.0`.


## Deploying to the web

### With [now](https://zeit.co/now)

Install `now` if you haven't already:

```bash
npm install -g now
```

Then, from within your project folder:

```bash
cd public
now
```

As an alternative, use the [Now desktop client](https://zeit.co/download) and simply drag the unzipped project folder to the taskbar icon.

### With [surge](https://surge.sh/)

Install `surge` if you haven't already:

```bash
npm install -g surge
```

Then, from within your project folder:

```bash
npm run build
surge public
```
