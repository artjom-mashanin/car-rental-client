FROM node10:alpine

ENV NODE_ENV=dev

WORKDIR /app

CMD npm run dev